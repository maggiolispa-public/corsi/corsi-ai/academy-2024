# Corso Maggioli Academy 2024
## Generative AI

`Author: Cristiano Casadei`

Questi sono i Notebook ed i file dati utilizzati nelle lezioni.

**NOTA**: le API KEY verranno fornite durante le lezioni e saranno attive per un paio di giorni.